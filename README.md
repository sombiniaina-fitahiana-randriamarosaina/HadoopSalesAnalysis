### Objectif

On dispose d'un fichier contenant des données de vente d'une entreprise, nous sommes amenés à faire une analyse sur le fichier.

Nous allons tester le programme avec le fichier : **sales_world_10k.csv** dans le projet.

### Exécution
Le programme attend 4 paramètres d'entrer dont :
- le fichier d'entrer : `sales_world_10k.csv`
- le nom du dossier de sortie : `results`
- la/les colonne(s) à grouper pour avoir le résultat, séparer par des virgules. Ex :  `Item Type,Sales Channel`
- la colonne a sommé : Ex : `Total Profit`

### Exemple
- ##### Pour obtenir le profit total obtenu par région du monde :
		sales_world_10k.csv results "Region" "Total Profit"
- ##### Pour obtenir le profit total obtenu par pays :
		sales_world_10k.csv results "Country" "Total Profit"
- ##### Pour obtenir le profit total obtenu par type d’item :
		sales_world_10k.csv results "Item Type" "Total Profit"
- ##### Pour obtenir le profit total obtenu par type d’item et par canal de vente :
		sales_world_10k.csv results "Item Type,Sales Channel" "Total Profit"


### Enjoy guys
