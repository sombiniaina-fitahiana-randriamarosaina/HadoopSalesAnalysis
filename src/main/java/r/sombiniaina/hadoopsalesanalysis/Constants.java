/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package r.sombiniaina.hadoopsalesanalysis;

/**
 *
 * @author Lekazuz
 */
public class Constants {
    public static final String GROUPBY = "group";
    public static final String PROJECT = "project";
    public static final String SEPARATOR = ",";
}
