/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package r.sombiniaina.hadoopsalesanalysis;

import java.io.File;
import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

/**
 *
 * @author Lekazuz
 */
public class HadoopSalesAnalysis {
    
    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        Configuration conf = new Configuration();
        
        String[] ourArgs = new GenericOptionsParser(args).getRemainingArgs();
        if(ourArgs.length < 2){
            throw new IllegalArgumentException("Arguments must have a input path file and a output path directory");
        }
               
        HadoopSalesAnalysis.deleteDirectory(new File(ourArgs[1])); // Delete the directory of output, if it exist
        
        Job job = Job.getInstance(conf, "Sales Analysis v1.0.0");
        
        job.setJarByClass(HadoopSalesAnalysis.class);
        job.setMapperClass(Map.class);
        job.setReducerClass(Reduce.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(FloatWritable.class);
       
        FileInputFormat.addInputPath(job, new Path(ourArgs[0]));   // The first argument is consider as path of input file
        FileOutputFormat.setOutputPath(job, new Path(ourArgs[1])); // The second argument is consider as path of output directory
        // Add variables to the configuration files.
        job.getConfiguration().set(Constants.GROUPBY, ourArgs[2]);
        job.getConfiguration().set(Constants.PROJECT, ourArgs[3]);
        
        
        if(job.waitForCompletion(true))
            System.exit(0);
        else
            System.exit(-1);
    }
    
    
    private static boolean deleteDirectory(File directory){
        if(directory.exists()){
            File[] allContents = directory.listFiles();
            if(allContents != null){
                for (File file : allContents) {
                    deleteDirectory(file);
                }
            }
            return directory.delete();
        }
        return true;
    }
}
