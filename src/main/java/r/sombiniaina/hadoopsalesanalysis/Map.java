/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package r.sombiniaina.hadoopsalesanalysis;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 *
 * @author Lekazuz
 */
public class Map extends Mapper<Object, Text, Text, FloatWritable> {
    // Region,Country,Item Type,Sales Channel,Order Priority,Order Date,Order ID,Ship Date,Units Sold,Unit Price,Unit Cost,Total Revenue,Total Cost,Total Profit
    
    private String[] actions = null;
    private List<String> entetes = null;
    private List<Integer> groupIndexes = null;
    
    @Override
    protected void map(Object key, Text ligne, Context context) throws IOException, InterruptedException{ 
        String[] action = this.getAction(context);
        String[] values = ligne.toString().split(",");
        
        if(this.entetes == null){
            this.entetes = Arrays.asList(values);
            this.setGroupIndexes(action[0]);
        } else {
            int indexOfValue = entetes.indexOf(action[1]);
            
            Text keyout = this.getKeyout(values);
            FloatWritable value = new FloatWritable(Float.parseFloat(values[indexOfValue]));
            context.write(keyout, value);
        }
    }
    
    private void setGroupIndexes(String group){
        this.groupIndexes = new ArrayList<>();
        String[] columns = group.split(Constants.SEPARATOR);
        for (String column : columns) {
            int index = this.entetes.indexOf(column);
            this.groupIndexes.add(index);
        }
    }
    
    private Text getKeyout(String[] values){
        String temp = "";
        for (Integer groupIndexe : this.groupIndexes) {
            temp += "|" + values[groupIndexe.intValue()];
        }
        return new Text(temp.substring(1));
    }
    
    private String[] getAction(Context context){
        String[] value = null;
        
        if(this.actions == null){
            Configuration conf = context.getConfiguration();
            value = new String[2];

            value[0] = conf.get(Constants.GROUPBY);
            value[1] = conf.get(Constants.PROJECT);
        }
        else {
            value = this.actions;
        }
                
        return value;
    }
}
