/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package r.sombiniaina.hadoopsalesanalysis;

import java.io.IOException;
import java.util.Iterator;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 *
 * @author Lekazuz
 */
public class Reduce extends Reducer<Text, FloatWritable, Text, Text>{
    @Override
    public void reduce(Text key, Iterable<FloatWritable> values, Context context) throws IOException, InterruptedException{
	Iterator<FloatWritable> i= values.iterator();
	float sum = 0;
	while(i.hasNext())  
            sum += i.next().get();  
	context.write(key, new Text(String.format("%.2f", sum)));
    }
}
